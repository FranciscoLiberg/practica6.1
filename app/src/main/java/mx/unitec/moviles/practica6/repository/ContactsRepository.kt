package mx.unitec.moviles.practica6.repository

import android.app.Application
import android.provider.ContactsContract
import androidx.lifecycle.LiveData
import mx.unitec.moviles.practica6.dao.ContacDao
import mx.unitec.moviles.practica6.db.ContactsDatabase
import mx.unitec.moviles.practica6.model.Contact

class ContactsRepository(application: Application) {
    private val contactDao: ContacDao? = ContactsDatabase.getInstance(application)?.contactDao()

    suspend fun insert(contact: Contact) {
        contactDao!!.insert(contact)
    }

    fun getContacts(): LiveData<List<Contact>> {
        return contactDao!!.getOrderedAgenda()
    }
}