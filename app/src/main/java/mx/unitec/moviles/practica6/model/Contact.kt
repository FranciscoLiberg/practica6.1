package mx.unitec.moviles.practica6.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity()
data class Contact(
    @ColumnInfo(name = "name") @NotNull val name: String,
    @ColumnInfo(name = "phone") @NotNull val phone: String,
    @ColumnInfo(name = "email") @NotNull val email: String) {

    companion object {
        const val TABLE_NAME = "contact"
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0


}